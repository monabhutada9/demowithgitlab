/**
 * Created by dmitry on 30.10.2020.
 */

public abstract class VT_R5_AbstractEDiaryAlertsBatch implements Database.Batchable<SObject>, Database.AllowsCallouts {

    public String test4; // Added for demo
    public String test5; // Added for demo
    public static final String NOTIFICATION_TYPE = 'eCOA Notification';
    public String NOTIFICATION_RECIPIENT_TYPE;
    public string test;
    public string test1;
    public string test2;

    private static final Id DAILY_ALERT_RECORD_TYPE_ID = Schema.SObjectType.VTR5_eDiaryNotificationBuilder__c.getRecordTypeInfosByDeveloperName().get('VTR5_DailyAlert').getRecordTypeId();
    private static final Id EXPIRATION_ALERT_RECORD_TYPE_ID = Schema.SObjectType.VTR5_eDiaryNotificationBuilder__c.getRecordTypeInfosByDeveloperName().get('VTR5_ExpirationAlert').getRecordTypeId();
    private static final Map<Integer, List<String>> OFFSET_TO_TIMEZONES_MAP = getTimezonesMap();

    protected Integer batchScopeSize;
    protected String scopeItemIdFieldName;
    protected Set<String> studyEDiaryKeys;
    protected Set<Datetime> eDiaryDueDates;
    protected Map<Id, VT_R5_eDiaryAlertsService.AlertRequestInfo> alertRequestsMap;
    protected Map<Id, Set<VTR5_eDiaryNotificationBuilder__c>> studyIdToBuildersMap;
    protected Map<Id, Set<String>> scopeItemIdToDiaryStudyKeyMap;
    protected Map<Id, Set<String>> scopeItemIdToAlertsKeyMap;

    public VT_R5_AbstractEDiaryAlertsBatch(Map<Id, VT_R5_eDiaryAlertsService.AlertRequestInfo> alertRequestsMap,String Recipient_Type) {
        this.batchScopeSize = 200;
        this.alertRequestsMap = alertRequestsMap;
        this.studyIdToBuildersMap = new Map<Id, Set<VTR5_eDiaryNotificationBuilder__c>>();
        this.studyEDiaryKeys = new Set<String>();
        this.eDiaryDueDates = new Set<Datetime>();
        this.scopeItemIdToDiaryStudyKeyMap = new Map<Id, Set<String>>();
        this.scopeItemIdToAlertsKeyMap = new Map<Id, Set<String>>();
        this.NOTIFICATION_RECIPIENT_TYPE = Recipient_Type;

        for(VTR5_eDiaryNotificationBuilder__c builder : getNotificationBuildersMap(alertRequestsMap.keySet())) {
            Id studyId = builder.VTR5_Study__c;
            VT_R5_eDiaryAlertsService.AlertRequestInfo alertRequestInfo = alertRequestsMap.get(builder.Id);

            if (!this.studyIdToBuildersMap.containsKey(studyId)) {
                this.studyIdToBuildersMap.put(studyId, new Set<VTR5_eDiaryNotificationBuilder__c>());
            }
            this.studyIdToBuildersMap.get(studyId).add(builder);

            if (alertRequestInfo.dueDate != null) {
                this.eDiaryDueDates.add(alertRequestInfo.dueDate);
            }

            this.studyEDiaryKeys.add(builder.VTR5_DiaryStudyKey__c);
        }
    }



    public Integer getBatchScopeSize() {
        return this.batchScopeSize;
    }

    public Iterable<SObject> start(Database.BatchableContext param1) {
        return getScope();
    }

    public void execute(Database.BatchableContext param1, List<SObject> scope) {
        Set<Id> scopeIds = getScopeIds(scope);

        for (AggregateResult ar : getAggregatedSurveys(scopeIds)) {
            addItemsToDiaryKeysMaps(ar, this.scopeItemIdFieldName);
        }
        
      //5.7 Epic: SH-20625 to send notification only to site staff with alteast one patient on associated site 
        if(NOTIFICATION_RECIPIENT_TYPE == 'Site_Staff'){
            Set<ID> virtualsiteIdSet = new Set<ID>();
            for(Virtual_Site__c vs:[Select Id from Virtual_Site__c where VTD1_Study__c in :scopeIds])
            {
            virtualsiteIdSet.add(vs.Id);
            }
           Set<Id> virtualsiteIdswithCase = new Set<Id>();
            for(AggregateResult ar : [
                SELECT VTD1_Virtual_Site__c,Count(Id) countOfRec  
                FROM Case 
                WHERE VTD1_Virtual_Site__c!=null  AND VTD1_Virtual_Site__c IN :virtualsiteIdSet  group by VTD1_Virtual_Site__c Having Count(Id) > 0 ])
            {
                  virtualsiteIdswithCase.add((Id) ar.get('VTD1_Virtual_Site__c')); 
            }
            
            Set<Id> piStmids = new Set<Id>();
            Set<Id> stmIdsSet = new Set<Id>();
           for(Virtual_Site__c vs : [Select VTD1_Study_Team_Member__c,VTR2_Backup_PI_STM__c from Virtual_Site__c where Id in :virtualsiteIdswithCase AND VTD1_Study_Team_Member__c != NULL]){
                piStmids.add(vs.VTD1_Study_Team_Member__c); 
               if(vs.VTR2_Backup_PI_STM__c!=null){
                stmIdsSet.add(vs.VTR2_Backup_PI_STM__c);  
                }   
           }           
           stmIdsSet.addAll(piStmids);        
           for(Study_Site_Team_Member__c sstm : [SELECT VTD1_Associated_PG__c,VTR2_Associated_SCr__c,VTR2_Associated_SubI__c 
                                                 FROM Study_Site_Team_Member__c 
                                                 where VTD1_Associated_PI__c in :piStmids OR VTR2_Associated_PI__c in :piStmids OR VTR2_Associated_PI3__c in :piStmids]){
                  if(sstm.VTD1_Associated_PG__c!=null){    
                  stmIdsSet.add(sstm.VTD1_Associated_PG__c);  
                  }    
                  if(sstm.VTR2_Associated_SCr__c!=null){                                   
                  stmIdsSet.add(sstm.VTR2_Associated_SCr__c); 
                  }    
                  if(sstm.VTR2_Associated_SubI__c!=null){                                                      
                  stmIdsSet.add(sstm.VTR2_Associated_SubI__c);  
                  }    
            } 
            
            List<SObject> scopeNotification = new List<SObject>();          
            for (SObject scopeItem : scope) {
                Study_Team_Member__c stm = (Study_Team_Member__c) scopeItem;
                if(stm.VTD1_Type__c == VT_R4_ConstantsHelper_ProfilesSTM.SC_PROFILE_NAME || stmIdsSet.contains(stm.Id) ){                   
                       scopeNotification.add(scopeItem);
                   }     
                }                
             scope.clear();
             scope.addAll(scopeNotification);                                             
        }

        for (SObject scopeItem : scope) {
            Id studyId = getStudyIdFromSObject(scopeItem);

            for (VTR5_eDiaryNotificationBuilder__c builder : this.studyIdToBuildersMap.get(studyId)) {
                VT_R5_eDiaryAlertsService.AlertRequestInfo alertRequestInfo = this.alertRequestsMap.get(builder.Id);

                if (builder.RecordTypeId == DAILY_ALERT_RECORD_TYPE_ID) {
                    prepareDailyAlerts(scopeItem, alertRequestInfo, builder);
                } else if (builder.RecordTypeId == EXPIRATION_ALERT_RECORD_TYPE_ID) {
                    prepareExpirationAlerts(scopeItem, alertRequestInfo, builder);
                }
            }
        }

        VT_R5_eDiaryAlertsBulkInsertRestService.RequestPayload alertsToInsert = getAlerts();
        if (alertsToInsert != null) {
            if (!Test.isRunningTest()) {     // Modified in R5.7 for epic# SH-20625 to ensure that from a test context the doPost() of VT_R5_eDiaryAlertsBulkInsertRestService is directly called, instead of making an Http reuqest callout 
            bulkAlertsInsert(alertsToInsert);
            }
            else{
                RestResponse response = new RestResponse();
                RestRequest request = new RestRequest();
                request.requestBody = Blob.valueOf(JSON.serialize(alertsToInsert));
                RestContext.request = request;
                RestContext.response = response;           
                VT_R5_eDiaryAlertsBulkInsertRestService.doPost();
            }
        }
    }


    

    public void finish(Database.BatchableContext param1) {}

    protected abstract Iterable<SObject> getScope();

    protected abstract Set<Id> getScopeIds(List<SObject> scope);

    protected abstract List<AggregateResult> getAggregatedSurveys(Set<Id> scopeIds);

    protected abstract void prepareDailyAlerts(SObject scopeItem, VT_R5_eDiaryAlertsService.AlertRequestInfo alertRequestInfo, VTR5_eDiaryNotificationBuilder__c builder);

    protected abstract void prepareExpirationAlerts(SObject scopeItem, VT_R5_eDiaryAlertsService.AlertRequestInfo alertRequestInfo, VTR5_eDiaryNotificationBuilder__c builder);

    protected abstract VT_R5_eDiaryAlertsBulkInsertRestService.RequestPayload getAlerts();

    protected Boolean isTimezoneMatched (Integer alertTzOffset, User u) {
        return OFFSET_TO_TIMEZONES_MAP.get(alertTzOffset).contains(u.TimeZoneSidKey);       
    }

    protected String getAlertKey (VT_R5_eDiaryAlertsService.AlertRequestInfo alertRequestInfo, VTR5_eDiaryNotificationBuilder__c builder) {
        return builder.VTR5_DiaryStudyKey__c + alertRequestInfo.dueDate.formatGmt('yyyy-MM-dd HH:mm:ss');
    }

    private void addItemsToDiaryKeysMaps(AggregateResult ar, String scopeItemIdField) {
        Id scopeItemId = Id.valueOf(String.valueOf(ar.get(scopeItemIdField)));
        String alertKey = String.valueOf(ar.get('VTR5_eCoaAlertsKey__c'));
        String diaryStudyKey = getEDiaryStudyKey(alertKey);

        if (!this.scopeItemIdToDiaryStudyKeyMap.containsKey(scopeItemId)) {
            this.scopeItemIdToDiaryStudyKeyMap.put(scopeItemId, new Set<String>());
        }
        this.scopeItemIdToDiaryStudyKeyMap.get(scopeItemId).add(diaryStudyKey);

        if (!this.scopeItemIdToAlertsKeyMap.containsKey(scopeItemId)) {
            this.scopeItemIdToAlertsKeyMap.put(scopeItemId, new Set<String>());
        }
        this.scopeItemIdToAlertsKeyMap.get(scopeItemId).add(alertKey);
    }

    private static Map<Integer, List<String>> getTimezonesMap() {
        Map<Integer, List<String>> offsetToTimezone = new Map<Integer, List<String>>();
        Datetime dt = Datetime.now();
        Schema.DescribeFieldResult fieldResult = User.fields.TimeZoneSidKey.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry pickListVal : ple) {
            Integer offset = TimeZone.getTimeZone(pickListVal.getValue()).getOffset(dt)/60000;
            if (!offsetToTimezone.containsKey(offset)) {
                offsetToTimezone.put(offset, new List<String>());
            }
            offsetToTimezone.get(offset).add(pickListVal.getValue());
        }
        return offsetToTimezone;
    }

    private static Id getStudyIdFromSObject(SObject record) {
        switch on String.valueOf(record.getSObjectType()) {
            when 'Case' {
                return (Id) record.get('VTD1_Study__c');
            }
            when 'Study_Team_Member__c' {
                return (Id) record.get('Study__c');
            }
            when else {
                return null;
            }
        }
    }

    private static List<VTR5_eDiaryNotificationBuilder__c> getNotificationBuildersMap(Set<Id> ids) {
        return [
                SELECT VTR5_AlertTime__c,
                        VTR5_eDiaryName__c,
                        VTR5_NotificationMessage__c,
                        VTR5_NotificationRecipient__c,
                        VTR5_Notification_Title__c,
                        VTR5_Offset__c,
                        VTR5_Study__c,
                        VTR5_DiaryStudyKey__c,
                        VTR5_OperationType__c,
                        VTR5_DiaryListView__c,
                        RecordTypeId
                FROM VTR5_eDiaryNotificationBuilder__c
                WHERE Id IN :ids
                AND VTR5_Active__c = TRUE
        ];
    }

    private static String getEDiaryStudyKey (String sourceString) {
        String regExp = '([0-9]{4})-([0-9]{2})-([0-9]{2})\\s([0-9]{2}):([0-9]{2}):([0-9]{2})';
        return sourceString.replaceAll('\\*-\\*', '').replaceAll(regExp, '');
    }

// Made TestVisible in R5.7 for epic# SH-20625 for test class coverage for bulkInsert() 
@TestVisible
    private static void bulkAlertsInsert(VT_R5_eDiaryAlertsBulkInsertRestService.RequestPayload requestBody) {
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
        req.setHeader('Content-Type', 'application/json');
        req.setEndpoint(System.Url.getOrgDomainUrl().toExternalForm() + '/services/apexrest/ediary-bulk-alerts');
        req.setBody(JSON.serialize(requestBody));
        req.setTimeout(120000);
        try {
            new Http().send(req);
        } catch (Exception e) {
            System.debug(e.getMessage());
        }
    }

}